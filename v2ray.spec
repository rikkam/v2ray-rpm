%global proj_name v2fly
%global geoip_ver 20220923014311

Name:           v2ray
Version:        4.45.2
Release:        1%{?dist}
Summary:        The core of the network forwarding toolkit Project V

License:        MIT
URL:            https://github.com/%{proj_name}/%{name}-core
Source0:        https://github.com/%{proj_name}/%{name}-core/archive/v%{version}/%{name}-core-v%{version}.tar.gz
Source1:        v2ray.xml
Source2:        https://github.com/%{proj_name}/geoip/releases/latest/download/geoip.dat
Source3:        https://github.com/%{proj_name}/domain-list-community/releases/latest/download/dlc.dat

Patch0:         systemd-unit-file.patch

BuildRequires:  golang >= 1.16-0
BuildRequires:  git >= 1.8.3.1-13
BuildRequires:  gcc
BuildRequires:  systemd-units
Requires:       systemd-units
Requires:       v2ray-geoip

%description
The core of Project V, which is a series of tools for network forwarding. 

%package firewalld
Summary: The example firewalld service file.
BuildArch: noarch
%description firewalld
The example firewalld service file.

%package geoip
Summary: The geoip data file.
BuildArch: noarch
%description geoip
The geoip data file.

%global debug_package %{nil}

%prep
%setup -q -n %{name}-core-%{version}
%patch0 -p1
cp %{_sourcedir}/%{name}.xml ./

%build
export GO111MODULE=on
go build -o v2ray -buildmode=pie -compiler gc -gcflags -trimpath=%{gopath} -asmflags -trimpath=%{gopath} -ldflags "-s -w" ./main
go build -o v2ctl -buildmode=pie -compiler gc -gcflags -trimpath=%{gopath} -asmflags -trimpath=%{gopath} -ldflags "-s -w" ./infra/control/main

%install
install -d %{buildroot}%{_bindir}/%{name}
install -d %{buildroot}%{_datadir}/%{name}
install -d %{buildroot}%{_sysconfdir}/%{name}
install -d %{buildroot}%{_usr}/lib/firewalld/services/
install -d %{buildroot}%{_localstatedir}/log/%{name}
install -d %{buildroot}%{_unitdir}

install -p -m 0755 %{name} %{buildroot}%{_bindir}/%{name}/%{name}
install -p -m 0755 v2ctl %{buildroot}%{_bindir}/%{name}/v2ctl
install -p -m 0644 $(pwd)/release/config/systemd/system/%{name}.service %{buildroot}%{_unitdir}/%{name}.service
install -p -m 0644 $(pwd)/release/config/systemd/system/%{name}@.service %{buildroot}%{_unitdir}/%{name}@.service

install -p -m 0644 $(pwd)/release/config/vpoint_vmess_freedom.json %{buildroot}%{_sysconfdir}/%{name}/config.json

install -p -m 0644 $(pwd)/v2ray.xml %{buildroot}%{_usr}/lib/firewalld/services/v2ray.xml

install -p -m 0644 %{_sourcedir}/geoip.dat %{buildroot}%{_datadir}/%{name}/geoip.dat
install -p -m 0644 %{_sourcedir}/dlc.dat %{buildroot}%{_datadir}/%{name}/geosite.dat

%post
[ -f "/usr/bin/v2ray/geoip.dat" ] && rm -f /usr/bin/v2ray/geoip.dat || true
[ -f "/usr/bin/v2ray/geosite.dat" ] && rm -f /usr/bin/v2ray/geosite.dat || true

%files
%defattr(-,root,root,-)
%license LICENSE
%{_bindir}/%{name}/%{name}
%{_bindir}/%{name}/v2ctl
%attr(-, root, root) %{_unitdir}/%{name}.service
%attr(-, root, root) %{_unitdir}/%{name}@.service
%config(noreplace) %{_sysconfdir}/%{name}/config.json

%files geoip
%{_datadir}/%{name}/geoip.dat
%{_datadir}/%{name}/geosite.dat

%files firewalld
%config(noreplace) %{_usr}/lib/firewalld/services/v2ray.xml


%changelog
